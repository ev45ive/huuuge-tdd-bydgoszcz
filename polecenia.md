git clone https://bitbucket.org/ev45ive/huuuge-tdd-bydgoszcz.git
npm i
npm start

##
git clone https://github.com/pzurowski/sages-tdd-angular/
##
npm i -g @angular/cli
##
ng new huuuge-testing
cd huuuge-testing
npm start

ng g module core -m app
ng g module auth -m app --routing

ng g c auth/login
ng g c auth/register

ng g s auth/auth

ng g m users -m app --routing
ng g c users/containers/browse-users
ng g c users/components/users-list
ng g c users/components/user-details

ng g m posts -m app --routing
ng g c posts/containers/browse-posts
ng g c posts/components/posts-list
ng g c posts/components/post-details

npm i --save ng-mocks

npm install @ngneat/spectator --save

## List schematics
ng g @ngneat/spectator:

## Generate 
ng g @ngneat/spectator:spectator-component posts/containers/post-comments
ng g @ngneat/spectator:spectator-component posts/components/comments-list --withHost=true

## Set Default
ng config cli.defaultCollection @ngneat/spectator

ng g cs posts/containers/post-comments
ng g cs posts/components/comments-list --withHost=true


npm install -D jest jest-preset-angular @types/jest

https://www.xfive.co/blog/testing-angular-faster-jest/
