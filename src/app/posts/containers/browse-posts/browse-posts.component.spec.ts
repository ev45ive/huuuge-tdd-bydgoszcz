import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { MockComponent, MockComponents, MockRender } from "ng-mocks";
import { BrowsePostsComponent } from "./browse-posts.component";
import { PostsListComponent } from "../../components/posts-list/posts-list.component";
import { PostDetailsComponent } from "../../components/post-details/post-details.component";
import { By } from "@angular/platform-browser";
import { Post } from "./Post";

describe("BrowsePostsComponent", () => {
  let component: BrowsePostsComponent;
  let fixture: ComponentFixture<BrowsePostsComponent>;
  let posts: Post[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BrowsePostsComponent,
        // MockComponent()
        ...MockComponents(PostsListComponent, PostDetailsComponent)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowsePostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    posts = [{ name: "Test 1" }, { name: "Test 2" }, { name: "Test 3" }];
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should have list", () => {
    const list = fixture.debugElement.query(By.css("app-posts-list"));
    expect(list).not.toBeNull();
  });

  it("provides posts data to list", () => {
    const list = fixture.debugElement.query(By.css("app-posts-list"))
      .componentInstance as PostsListComponent;

    // Update parent data and detectChanges
    component.posts = posts;
    fixture.detectChanges();

    // Check child inputs
    expect(list.posts).toEqual(posts);
  });

  it("should get current post selection", () => {
    const list = fixture.debugElement.query(By.css("app-posts-list"))
      .componentInstance as PostsListComponent;

    const spy = spyOn(component, "select");
    list.selectedChange.emit(posts[1]);

    expect(spy).toHaveBeenCalledWith(posts[1]);
  });

  it("should have details", () => {
    const details = fixture.debugElement.query(By.css("app-post-details"));
    expect(details).not.toBeNull();
  });

  it("should select post details", () => {
    const details = fixture.debugElement.query(By.css("app-post-details"))
      .componentInstance as PostDetailsComponent;

    component.select(posts[1]);
    fixture.detectChanges();

    expect(details.post).toEqual(posts[1]);
  });
});
