import { Component, OnInit } from "@angular/core";
import { Post } from "./Post";

@Component({
  selector: "app-browse-posts",
  templateUrl: "./browse-posts.component.html",
  styleUrls: ["./browse-posts.component.scss"]
})
export class BrowsePostsComponent implements OnInit {
  selected: Post;
  posts: Post[];

  constructor() {}

  select(post: Post) {
    this.selected = post;
  }

  ngOnInit() {}
}
