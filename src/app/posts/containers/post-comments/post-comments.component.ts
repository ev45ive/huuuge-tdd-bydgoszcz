import { Component, OnInit, Input } from "@angular/core";
import { PostComment } from "./PostComment";

@Component({
  selector: "app-post-comments",
  templateUrl: "./post-comments.component.html",
  styleUrls: ["./post-comments.component.css"]
})
export class PostCommentsComponent implements OnInit {
  
  select(comment: PostComment) {}

  comments: PostComment[];

  constructor() {}

  ngOnInit() {}
}
