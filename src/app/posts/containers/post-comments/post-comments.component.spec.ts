import { Spectator, createComponentFactory } from "@ngneat/spectator";

import { PostCommentsComponent } from "./post-comments.component";
import { MockComponent } from "ng-mocks";
import { CommentsListComponent } from "../../components/comments-list/comments-list.component";
import { PostComment } from './PostComment';
import { EventEmitter } from '@angular/core';

describe("PostCommentsComponent", () => {
  let spectator: Spectator<PostCommentsComponent>;
  let mock = MockComponent(CommentsListComponent);
  let comments: PostComment[];

  const createComponent = createComponentFactory({
    component: PostCommentsComponent,
    declarations: [mock]
  });

  beforeEach(() => {
    spectator = createComponent();
    comments = [{}, {}, {}];
  });

  it("should create", () => {
    expect(spectator.component).toBeTruthy();
  });

  it("should render component with comments list ", () => {
    // const list = spectator.query("app-comments-list", {
    //   read: CommentsListComponent
    // });
    const list = spectator.query(CommentsListComponent);
    
    spectator.component.comments = comments;
    spectator.detectChanges()

    expect(list.comments).toEqual(comments);
  });

  it('should get selected comment',()=>{
    const list = spectator.query(CommentsListComponent);
    
    const spy = spyOn(spectator.component,'select')

    list.selectedChange.emit(comments[2])

    expect(spy).toHaveBeenCalledWith(comments[2])
  })

});
