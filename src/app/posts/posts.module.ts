import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { BrowsePostsComponent } from './containers/browse-posts/browse-posts.component';
import { PostsListComponent } from './components/posts-list/posts-list.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { PostCommentsComponent } from './containers/post-comments/post-comments.component';
import { CommentsListComponent } from './components/comments-list/comments-list.component';


@NgModule({
  declarations: [BrowsePostsComponent, PostsListComponent, PostDetailsComponent, PostCommentsComponent, CommentsListComponent],
  imports: [
    CommonModule,
    PostsRoutingModule
  ]
})
export class PostsModule { }
