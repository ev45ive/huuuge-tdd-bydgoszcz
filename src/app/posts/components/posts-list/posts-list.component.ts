import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Post } from "../../containers/browse-posts/Post";

@Component({
  selector: "app-posts-list",
  templateUrl: "./posts-list.component.html",
  styleUrls: ["./posts-list.component.scss"]
})
export class PostsListComponent implements OnInit {
  @Input()
  selected: Post;

  @Input()
  posts: Post[];

  @Output()
  selectedChange = new EventEmitter();

  select(post: Post) {
    this.selectedChange.emit(post);
  }

  constructor() {}

  ngOnInit() {}
}
