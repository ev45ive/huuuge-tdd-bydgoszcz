import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { PostsListComponent } from "./posts-list.component";
import { MockRender } from "ng-mocks";
import { By } from "@angular/platform-browser";
import { Post } from "../../containers/browse-posts/Post";

describe("PostsListComponent", () => {
  let component: PostsListComponent;
  let fixture: ComponentFixture<PostsListComponent>;
  let posts: Post[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostsListComponent]
    }).compileComponents();
    posts = [{ name: "Test 1" }, { name: "Test 2" }, { name: "Test 3" }];
  }));

  it("should take a list of posts", () => {
    const host = MockRender(
      `<app-posts-list [posts]="list"></app-posts-list>`,
      {
        list: posts
      }
    );
    const debugElem = host.debugElement.query(By.css("app-posts-list"));
    component = debugElem.componentInstance;

    expect(component.posts).toEqual(posts);
  });
  it("should take  selected posts", () => {
    const host = MockRender(
      `<app-posts-list [selected]="selected"></app-posts-list>`,
      {
        selected: posts[2]
      }
    );
    const debugElem = host.debugElement.query(By.css("app-posts-list"));
    component = debugElem.componentInstance;

    expect(component.selected).toEqual(posts[2]);
  });

  it("should emit selected post", () => {
    const spy = jasmine.createSpy("postSelected");

    const host = MockRender(
      `<app-posts-list (selectedChange)="spy($event)"></app-posts-list>`,
      {
        spy
      }
    );
    const debugElem = host.debugElement.query(By.css("app-posts-list"));
    component = debugElem.componentInstance;
    component.select({} as Post);

    expect(spy).toHaveBeenCalledWith({});
  });

  it("should have no posts", () => {
    const fixture = TestBed.createComponent(PostsListComponent);
    fixture.detectChanges();

    const noPosts = fixture.debugElement.queryAll(By.css(".post-list-item"));
    expect(noPosts.length).toEqual(0);
  });

  it("should display list of posts", () => {
    const fixture = TestBed.createComponent(PostsListComponent);
    const component = fixture.componentInstance;

    component.posts = posts;
    fixture.detectChanges();
    const somePosts = fixture.debugElement.queryAll(By.css(".post-list-item"));

    expect(somePosts.length).toEqual(3);
    expect(somePosts[0].nativeElement.textContent).toMatch(posts[0].name);
    expect(somePosts[1].nativeElement.textContent).toMatch(posts[1].name);
    expect(somePosts[2].nativeElement.textContent).toMatch(posts[2].name);
  });

  it("should select clicked post", () => {
    const fixture = TestBed.createComponent(PostsListComponent);
    const component = fixture.componentInstance;
    const spy = spyOn(component, "select");
    component.posts = posts;
    fixture.detectChanges();
    const somePosts = fixture.debugElement.queryAll(By.css(".post-list-item"));

    somePosts[2].triggerEventHandler("click", {});

    expect(spy).toHaveBeenCalledWith(posts[2]);
  });

  it("shoud highlight selected post", () => {
    const fixture = TestBed.createComponent(PostsListComponent);
    const component = fixture.componentInstance;
    
    component.posts = posts;
    component.selected = posts[1]

    fixture.detectChanges();
    const somePosts = fixture.debugElement.queryAll(By.css(".post-list-item"));

    expect(somePosts[0].classes["active"]).toBeFalsy();
    expect(somePosts[1].classes["active"]).toBeTruthy();
    expect(somePosts[2].classes["active"]).toBeFalsy();
  });
});
