import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { PostComment } from "../../containers/post-comments/PostComment";

@Component({
  selector: "app-comments-list",
  templateUrl: "./comments-list.component.html",
  styleUrls: ["./comments-list.component.css"]
})
export class CommentsListComponent implements OnInit {
  select(comment: PostComment) {
    this.selectedChange.emit(comment)
  }

  @Input()
  comments: PostComment[];

  @Output()
  selectedChange = new EventEmitter<PostComment>();

  constructor() {}

  ngOnInit() {}
}
