import {
  createHostFactory,
  SpectatorHost,
  HostComponent,
  createComponentFactory
} from "@ngneat/spectator";

import { CommentsListComponent } from "./comments-list.component";
import { PostComment } from "../../containers/post-comments/PostComment";

describe("CommentsListComponent", () => {
  let spectator: SpectatorHost<CommentsListComponent>;
  let comments: PostComment[];

  // const createHost = createHostFactory(CommentsListComponent);
  const createComponent = createComponentFactory(CommentsListComponent);

  beforeEach(() => {
    comments = [{}, {}, {}];
  });

  // it("should create", () => {
  //   spectator = createHost(
  //     `<app-comments-list [comments]="comments"></app-comments-list>`,
  //     {
  //       hostProps: {
  //         comments
  //       }
  //     }
  //   );
  //   // spectator.output('selectedChange')
  //   expect(spectator.component.comments).toEqual(comments);
  // });

  it("should inputs", () => {
    const spectator = createComponent();
    spectator.setInput("comments", comments);
    expect(spectator.component.comments).toEqual(comments);
  });

  it("should outputs", () => {
    const spectator = createComponent();
    const spy = jasmine.createSpy("SelectedChange");
    spectator.output("selectedChange").subscribe(spy);
    spectator.component.select(comments[2]);
    expect(spy).toHaveBeenCalledWith(comments[2]);
  });

  // it('should...', () => {
  //   spectator = createHost(`<zippy title="Zippy title">Zippy content</zippy>`);
  //   spectator.click('.zippy__title');
  //   expect(spectator.query('.arrow')).toHaveText('Close');
  // });

  // it('should...', () => {
  //   spectator = createHost(`<zippy title="Zippy title"></zippy>`);
  //   spectator.click('.zippy__title');
  //   expect(host.query('.zippy__content')).toExist();
  //   spectator.click('.zippy__title');
  //   expect('.zippy__content').not.toExist();
  // });
});
