import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { BrowseUsersComponent } from "./browse-users.component";
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Component,
  EventEmitter
} from "@angular/core";
import { By } from "@angular/platform-browser";
import { UsersListComponent } from "../../components/users-list/users-list.component";

@Component({
  selector: "app-users-list",
  template: ``
})
class MockListComponent extends UsersListComponent {
  // @Output()
  // userSelected = new EventEmitter();
}

@Component({
  selector: "app-user-details",
  template: `
    <ng-content></ng-content>
  `
})
class MockDetailsComponent {}

describe("BrowseUsersComponent", () => {
  let component: BrowseUsersComponent;
  let fixture: ComponentFixture<BrowseUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BrowseUsersComponent,
        MockListComponent,
        MockDetailsComponent
      ],
      schemas: [
        // NO_ERRORS_SCHEMA
        // CUSTOM_ELEMENTS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should have users list", () => {
    const usersElem = fixture.debugElement.query(
      By.directive(MockListComponent)
    );
    // const usersElem = fixture.debugElement.query(By.css("app-users-list"));
    const component = usersElem.componentInstance as UsersListComponent;
    expect(component.users).toEqual([1, 2, 3]);
  });

  it("should emit selected user from userlist", () => {
    const usersElem = fixture.debugElement.query(
      By.directive(MockListComponent)
    );
    const spy = spyOn(component, "selectUser");
    const mock = usersElem.componentInstance as UsersListComponent;
    mock.userSelected.emit("TestUser");

    expect(spy).toHaveBeenCalledWith("TestUser");
  });


  // it("should have users list", () => {
  //   const usersElem = fixture.debugElement.query(By.css("app-users-list"));
  //   // usersElem.
  //   usersElem.listeners
  //     .find(l => l.name == "userSelected")
  //     .callback("Zbyszek!");
  //   debugger;
  //   expect(usersElem).not.toBeNull();
  // });
});
