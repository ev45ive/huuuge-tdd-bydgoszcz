import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";

@Component({
  selector: "app-users-list",
  templateUrl: "./users-list.component.html",
  styleUrls: ["./users-list.component.scss"],
})
export class UsersListComponent implements OnInit {
  selectUser(selectUser: any) {
    throw new Error("Method not implemented.");
  }

  @Output()
  userSelected = new EventEmitter();

  @Input()
  users: any;

  constructor() {}

  ngOnInit() {}
}
