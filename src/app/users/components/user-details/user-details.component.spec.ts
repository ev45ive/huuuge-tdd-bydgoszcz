import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import {
  UserDetailsComponent,
  CloseButtonDirective
} from "./user-details.component";
import {
  Component,
  Directive,
  forwardRef,
  EventEmitter,
  Output
} from "@angular/core";
import { By } from "@angular/platform-browser";

@Directive({
  selector: "[close-button]",
  providers: [
    {
      provide: CloseButtonDirective,
      useExisting: forwardRef(() => MockCloseButtonDirective)
    }
  ]
})
export class MockCloseButtonDirective {
  @Output()
  close = new EventEmitter();
}

@Component({
  template: `
    <app-user-details>
      <button close-button>Test</button>
    </app-user-details>
  `
})
export class UserDetailsComponentHOST {}

describe("UserDetailsComponent", () => {
  let component: UserDetailsComponentHOST;
  let fixture: ComponentFixture<UserDetailsComponentHOST>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserDetailsComponent,
        UserDetailsComponentHOST,
        MockCloseButtonDirective
      ]
    }).compileComponents();
    console.log("outer beforeEach");
  }));

  // describe("", () => {
  //   beforeEach(() => {
  //     console.log("inner beforeEach");
  //   });
  //   it("inner it", () => {
  //     expect(true).toBeTruthy();
  //   });
  //   it("inner it", () => {
  //     expect(true).toBeTruthy();
  //   });
  // });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsComponentHOST);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should detect content child emits", () => {
    const details = fixture.debugElement.query(
      By.directive(UserDetailsComponent)
    );
    const spy = spyOn(details.componentInstance, "close");

    const closeBtn = fixture.debugElement
      .query(By.directive(MockCloseButtonDirective))
      .injector.get(CloseButtonDirective);

    closeBtn.close.emit();
    expect(spy).toHaveBeenCalled();
  });
});
