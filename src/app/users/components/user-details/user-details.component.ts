import {
  Component,
  OnInit,
  ContentChild,
  Directive,
  Output,
  EventEmitter,
  TemplateRef
} from "@angular/core";

@Directive({
  selector: "[close-button]"
})
export class CloseButtonDirective {
  @Output()
  close = new EventEmitter();
}

@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.scss"]
})
export class UserDetailsComponent implements OnInit {
  @ContentChild(CloseButtonDirective, {
    static: true
    // read: TemplateRef
  })
  closebutton: CloseButtonDirective;

  ngAfterContentInit() {
    if (this.closebutton) this.closebutton.close.subscribe(() => this.close());
  }

  close() {}

  constructor() {}

  ngOnInit() {}
}
