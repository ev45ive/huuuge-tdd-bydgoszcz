import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  changeDetection:ChangeDetectionStrategy.Default
})
export class AppComponent {
  title = "huuuge-testing";

  counter = 0;

  constructor(
    private cdr: ChangeDetectorRef
  ) {
    // cdr.detach()

    setInterval(() => {
      this.counter++;
      
      // cdr.detectChanges()
    }, 500);
  }
}
