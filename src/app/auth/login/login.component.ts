import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  showMessage(message: string) {
    this.message = message;
    setTimeout(() => {
      this.message = "";
    }, 3000);
  }

  login() {
    this.service
      .login(this.username, this.password)
      .subscribe(({ success }) => {
        this.message = success ? "Login Success" : "Login Failed";
      });
  }

  message = "";
  username: any;
  password: any;

  constructor(private service: AuthService) {}

  ngOnInit() {}
}
