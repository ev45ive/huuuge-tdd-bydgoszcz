import {
  async,
  ComponentFixture,
  TestBed,
  inject,
  tick,
  fakeAsync
} from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { LoginComponent } from "./login.component";
import { By } from "@angular/platform-browser";
import { AuthService } from "../auth.service";
import { Subject, EMPTY } from "rxjs";

export const data_fixtures = {
  messages: {
    LOGIN_SUCCESS: "Login Success",
    LOGIN_FAILED: "Login Failed"
  }
};

export const page_object = {
  // messagesEl = () => (fixture.query(By.css('.notification')))
};

fdescribe("LoginComponent", function() {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;

  // stub
  // jasmine.createSpyObj("AuthService", ["login"]);

  // mock
  // spyOn(authService, ["login"]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [FormsModule],
      providers: [
        {
          provide: AuthService,
          useValue: jasmine.createSpyObj("AuthService", ["login"])
        }
      ],
      schemas: []
    }).compileComponents();
  }));

  beforeEach(() => {
    authService = TestBed.get(AuthService);
  });

  // beforeEach(inject([AuthService], _authService_ => {
  //   authService = _authService_;
  // }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should work", () => {
    expect(component).toBeDefined();
  });

  it("should not have initial message", () => {
    const messageElem = fixture.debugElement.query(By.css("p.message"));
    expect(messageElem).not.toBeNull("Message Element does not exist");
    expect(messageElem.nativeElement.innerText).toMatch("");
  });

  it("should render updated message", () => {
    const messageElem = fixture.debugElement.query(By.css("p.message"));
    component.message = data_fixtures.messages.LOGIN_SUCCESS;

    fixture.detectChanges();
    expect(messageElem.nativeElement.innerText).toMatch(
      data_fixtures.messages.LOGIN_SUCCESS
    );
  });

  it("should obtain username ", () => {
    const usernameElem = fixture.debugElement.query(By.css("input.username"));
    expect(usernameElem).not.toBeNull();

    usernameElem.triggerEventHandler("input", {
      target: { value: "TestUser" }
    });

    expect(component.username).toEqual("TestUser");
  });

  it("should obtain password", () => {
    const passwordElem = fixture.debugElement.query(By.css("input.password"));
    expect(passwordElem).not.toBeNull();

    passwordElem.nativeElement.value = "TestPassword";
    passwordElem.nativeElement.dispatchEvent(new Event("input"));

    expect(component.password).toEqual("TestPassword");
  });

  it("should login when button is pressed", () => {
    const loginBtn = fixture.debugElement.query(By.css("input.login"));
    expect(loginBtn).not.toBeNull("No login button");

    const loginSpy = spyOn(component, "login");

    // loginBtn.nativeElement.click()
    loginBtn.triggerEventHandler("click", {});

    expect(loginSpy).toHaveBeenCalled();
  });

  // it(inject([AuthService], authService => {}));

  it("should send login request", () => {
    const spy = (authService as jasmine.SpyObj<
      AuthService
    >).login.and.returnValue(EMPTY);

    component.username = "TestUser";
    component.password = "TestPassword";
    component.login();

    expect(spy).toHaveBeenCalledWith("TestUser", "TestPassword");
  });

  it("should recieve auth response after login ", () => {
    const fakeResp = new Subject<{ success: boolean }>();

    (authService as jasmine.SpyObj<AuthService>).login.and.returnValue(
      fakeResp
    );

    component.login();
    fakeResp.next({ success: true });
    expect(component.message).toEqual("Login Success");

    component.login();
    fakeResp.next({ success: false });
    expect(component.message).toEqual("Login Failed");
  });

  it("should hide message after 3sec", fakeAsync(() => {
    const messageEl = fixture.debugElement.query(By.css(".message"));

    component.showMessage("Placki!");
    fixture.detectChanges();
    expect(messageEl.nativeElement.textContent).toMatch("Placki!");

    tick(2000);
    fixture.detectChanges();
    expect(messageEl.nativeElement.textContent).toMatch("Placki!");

    tick(1000);
    fixture.detectChanges();
    expect(messageEl.nativeElement.textContent).toEqual("");
  }));
});
