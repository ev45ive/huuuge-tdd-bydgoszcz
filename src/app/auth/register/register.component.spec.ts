import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync
} from "@angular/core/testing";

import { RegisterComponent } from "./register.component";
import {
  createComponentFactory,
  Spectator,
  byLabel,
  createServiceFactory,
  SpectatorService,
  createRoutingFactory,
  SpyObject
} from "@ngneat/spectator";
import { FormsModule } from "@angular/forms";
import { AuthService } from "../auth.service";
import { EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { EMPTY } from "rxjs";
import { LoginComponent } from "../login/login.component";
import { MockComponent } from "ng-mocks";

fdescribe("RegisterComponent", () => {
  const createComponent = createRoutingFactory({
    component: RegisterComponent,
    // declarations: [LoginComponent, RegisterComponent],
    imports: [FormsModule]
    // stubsEnabled: false,
    // routes: [
    //   { path: "register", component: RegisterComponent },
    //   { path: "login", component: LoginComponent }
    // ]
  });
  // const createComponent = createComponentFactory({
  //   component: RegisterComponent,
  //   imports: [FormsModule],
  //   providers: [
  //     {
  //       provide: Router,
  //       useValue: jasmine.createSpyObj<Router>("Router", ["navigate"])
  //     }
  //   ]
  // });
  // const createService = createServiceFactory(AuthService);

  let spectator: Spectator<RegisterComponent>;
  // let spectatorAuthService: SpectatorService<AuthService>;
  let spectatorAuthService: AuthService;

  beforeEach(() => {
    // spectatorAuthService = createService();
    spectator = createComponent();
    spectatorAuthService = spectator.get(AuthService);
  });

  it("should create", () => {
    expect(spectator.component).toBeTruthy();
  });

  it("should obtain username", () => {
    expect(spectator.query(byLabel("Username:"))).toExist();
    spectator.typeInElement(
      "TestUsername",
      spectator.query(byLabel("Username:"))
    );
    expect(spectator.component.username).toEqual("TestUsername");
  });

  it("should obtain password", () => {
    expect("input.password").toExist();
    spectator.typeInElement("Testpassword", "input.password");
    expect(spectator.component.password).toEqual("Testpassword");
  });

  it("should register when button clicked", () => {
    expect(".register").toExist();
    const spy = spyOn(spectator.component, "register");

    spectator.click(".register");
    expect(spy).toHaveBeenCalled();
  });

  it("should request registration from service", () => {
    spyOn(spectatorAuthService, "register").and.returnValue(EMPTY);
    spectator.component.username = "TEstUser";
    spectator.component.password = "Testpassword";

    spectator.component.register();
    expect(spectatorAuthService.register).toHaveBeenCalledWith(
      "TEstUser",
      "Testpassword"
    );
  });

  it("should redirect to login after regiestration success ", () => {
    const fakeResult = new EventEmitter<{ success: boolean }>();

    spyOn(spectatorAuthService, "register").and.returnValue(fakeResult);

    spectator.component.register();

    fakeResult.emit({ success: true });
    // tick();
    const router = spectator.get(Router);

    expect(router.navigate).toHaveBeenCalledWith(["/login"]);
  });

  // it('should show error after regiestration failed ',()=>{})
});

// https:// tiny.pl /tk d1 d
