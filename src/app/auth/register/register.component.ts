import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  username: any;
  password: any;

  register() {
    this.authService.register(this.username, this.password).subscribe(res => {
      // debugger
      if (res.success) {
        this.router.navigate(["/login"]);
      }
    });
  }

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {}
}
